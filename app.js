var express       = require('express');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var mongoose      = require('mongoose');
var passport      = require('passport');
var session       = require('express-session');
var flash         = require('connect-flash');

var authenticate  = require('./config/authenticate.js');
//DATABASE
var db            = require('./config/db_au.js');

// CONNECT DATABASE
var url           = db.mongoUrl; 
mongoose.connect(url);
var db            = mongoose.connection;

// ROUTE
var index         = require('./routes/index');
var userRouter    = require('./routes/userRouter');
var aodaiRouter   = require('./routes/aodaiRouter');
var dressRouter   = require('./routes/dressRouter');
var shirtRouter   = require('./routes/shirtsRouter');
var trouserRouter = require('./routes/trousersRouter');

db.on('error', console.error.bind(console, 'Connection Error: '));
db.once('open', function() {
  console.log('Connect Server !');
});

var app           = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//COOKIE
app.use(cookieParser());

// SESSION REQUIRED FOR PASSPORT

app.use(session({
  secret : '12345-67890-09876-54321-abcde',
  resave : true,
  saveUninitialized : true
}));

// PASSPORT
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); // FLASH MESSENGER

app.use(express.static(path.join(__dirname, 'public')));

//Router

app.use('/',          index);
app.use('/aodai',     aodaiRouter);
app.use('/dress',     dressRouter);
app.use('/shirts',    shirtRouter);
app.use('/trousers',  trouserRouter);
app.use('/users',     userRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// HTTPS
app.all('*', function(req, res, next) {
  console.log('req start: ', req.secure, req.hostname, req.url, app.get('secPort'));
  if ( req.secure ) {
    return next();
  };

  res.redirect('https://' + req.hostname + ':' + app.get('secPort') + req.url);

});

module.exports = app;
