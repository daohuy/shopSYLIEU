var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
    postBy : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    comment : {
        type : String,
        required : true
    },
    postAt : {

        type : Date,
        default : Date.now
    }
});

var comments = mongoose.model('comment', commentSchema);


module.exports = comments;