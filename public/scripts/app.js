'use strict';

var app = angular.module('myApp', ['ui.router', 'ngResource']);

app.run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeSuccess', function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
});

app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise('/home');
    $stateProvider
        //  ROUTE HOME
        .state('home', {
            url: '/home',
            views: {
                'top': {
                    templateUrl: 'views/top.html'
                },
                'footer': {
                    templateUrl: 'views/footer.html'
                },
                'content': {
                    templateUrl: 'views/home.html'
                }
            }
        })
        // === ROUTE FOR ALL DRESS ========================
        .state('home.dress', {
            url: '/dress',
            views: {
                'content@': {
                    templateUrl: 'views/menu_dress.html',
                    controller: 'dressController'
                }
            }
        })

        // config before run with ID

        .state('home.dress.detail', {
            url: '/:id',
            views: {
                'content@': {
                    templateUrl: 'views/detail_dress.html',
                    controller: 'dressController'
                }
            }
        })

        // === ROUTE FOR ALL AODAI ======================
        .state('home.aodai', {
            url: '/aodai',
            views: {
                'content@': {
                    templateUrl: 'views/menu_aodai.html',
                    controller: 'aodaiController'
                }
            }
        })

        // config before run with ID

        .state('home.aodai.detail', {
            url: '/:id',
            views: {
                'content@': {
                    templateUrl: 'views/detail_aodai.html',
                    controller: 'aodaiController'
                }
            }
        })

        // ====== ROUTE FOR ALL SHIRTS ==================
        .state('home.shirts', {
            url: '/shirts',
            views: {
                'content@': {
                    templateUrl: 'views/menu_shirts.html',
                    controller: 'shirtsController'
                }
            }
        })

        // config before run with ID

        .state('home.shirts.detail', {
            url: '/:id',
            views: {
                'content@': {
                    templateUrl: 'views/detail_shirt.html',
                    controller: 'shirtsController'
                }
            }
        })
        // === ROUTE FOR ALL TROUSERS ====================
        .state('home.trousers', {
            url: '/trousers',
            views: {
                'content@': {
                    templateUrl: 'views/menu_trousers.html',
                    controller: 'trousersController'
                }
            }
        })

        // config before run with ID

        .state('home.trousers.detail', {
            url: '/:id',
            views: {
                'content@': {
                    templateUrl: 'views/detail_trouser.html',
                    controller: 'trousersController'
                }
            }
        })
        
        .state('home.user', {
            url : '/user',
            views : {
                'content@' : {
                    templateUrl : 'views/users.html',
                    controller : 'userController'
                }    
            }
        })
        
    ;
});


