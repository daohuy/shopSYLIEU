// MAIN APP
var app = angular.module('myApp');

app.controller('aodaiController', ['aodaiFactory', '$scope','$stateParams', function (aodaiFactory, $scope, $stateParams) {
    //dreses is one dress
    $scope.aodais = {};
    $scope.aodai = {};
    $scope.rating = [];
    $scope.message = 'Now Loading ...';
    
    aodaiFactory.query(function(res) {
        // dress IS ALL DRESS
        $scope.aodais = res;
        //console.log($scope.dress);
    }, function(res) {
        res.statusText;
        //console.log(res.statusText);
    });
    
    
    $scope.aodai = aodaiFactory.get({
        aodaiId: $stateParams.id
    }).$promise.then(
        function(res) {
            $scope.aodai = res;
            console.log($scope.dress);
            //console.log($scope.dreses.image_OL);
            for ( i = 0 ; i < $scope.aodai.rate ; i++ ) {
                //console.log(i);
                $scope.rating.push(i);
            };
            //console.log($scope.rating);
            
        },
        function(res) {
            $scope.message = 'Error: ' + res.status + '' + res.statusText; 
            //console.log($scope.message);
        }
    );
    
}]);


app.controller('dressController', ['dressFactory', '$scope','$stateParams', function (dressFactory, $scope,$stateParams) {
    //dreses is one dress
    $scope.dresses = {};
    $scope.dress = {};
    $scope.rating = [];
    $scope.message = 'Now Loading ...';
    
    dressFactory.query(function(res) {
        // dress IS ALL DRESS
        $scope.dresses = res;
        //console.log($scope.dress);
    }, function(res) {
        res.statusText;
        //console.log(res.statusText);
    });
    
    
    $scope.dress = dressFactory.get({
        dressId: $stateParams.id
    }).$promise.then(
        function(res) {
            $scope.dress = res;
            console.log($scope.dress);
            //console.log($scope.dreses.image_OL);
            for ( i = 0 ; i < $scope.dress.rate ; i++ ) {
                //console.log(i);
                $scope.rating.push(i);
            };
            //console.log($scope.rating);
            
        },
        function(res) {
            $scope.message = 'Error: ' + res.status + '' + res.statusText; 
            //console.log($scope.message);
        }
    );
    
    
}]);

app.controller('shirtsController', ['shirtsFactory', '$scope','$stateParams', function (shirtsFactory, $scope, $stateParams) {
    //shirt is one dress
    $scope.shirts = {};
    $scope.shirt = {};
    $scope.rating = [];
    $scope.message = 'Now Loading ...';
    
    shirtsFactory.query(function(res) {
        // dress IS ALL DRESS
        $scope.shirts = res;
        //console.log($scope.dress);
    }, function(res) {
        res.statusText;
        //console.log(res.statusText);
    });
    
    
    $scope.shirt = shirtsFactory.get({
        shirtId: $stateParams.id
    }).$promise.then(
        function(res) {
            $scope.shirt = res;
            console.log($scope.shirt);
            //console.log($scope.dreses.image_OL);
            for ( i = 0 ; i < $scope.shirt.rate ; i++ ) {
                //console.log(i);
                $scope.rating.push(i);
            };
            //console.log($scope.rating);
            
        },
        function(res) {
            $scope.message = 'Error: ' + res.status + '' + res.statusText; 
            //console.log($scope.message);
        }
    );
}]);

app.controller('trousersController', ['trousersFactory', '$scope','$stateParams', function (trousersFactory, $scope, $stateParams) {
    //trousers is one dress
    $scope.trousers = {};
    $scope.trouser = {};
    $scope.rating = [];
    $scope.message = 'Now Loading ...';
    
    trousersFactory.query(function(res) {
        // dress IS ALL DRESS
        $scope.trousers = res;
        //console.log($scope.dress);
    }, function(res) {
        res.statusText;
        //console.log(res.statusText);
    });
    
    
    $scope.trouser = trousersFactory.get({
        trouserId: $stateParams.id
    }).$promise.then(
        function(res) {
            $scope.trouser = res;
            console.log($scope.trouser);
            //console.log($scope.dreses.image_OL);
            for ( i = 0 ; i < $scope.trouser.rate ; i++ ) {
                //console.log(i);
                $scope.rating.push(i);
            };
            //console.log($scope.rating);
            
        },
        function(res) {
            $scope.message = 'Error: ' + res.status + '' + res.statusText; 
            //console.log($scope.message);
        }
    );
}]);

app.controller('userController', ['userFactory','$scope', '$stateParams', function(userFactory, $scope, $stateParams) {
    $scope.user = {
        'username' : '',
        'password' : ''
    };
    
    $scope.loginUser = function() {
        userFactory.save($scope.user, function(res) {
            console.log(res);
        } )
    };
}]);