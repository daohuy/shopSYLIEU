'use strict';

var app = angular.module('myApp');
app.constant("baseURL", "https://localhost:3443/");

app.factory('aodaiFactory', ['$resource', 'baseURL', function ($resource, baseURL) {
    return $resource(baseURL + "aodai/:aodaiId", {
        aodaiId: "@aodaiId"
    }, {
        update: {
            method: "PUT"
        }
    });
}]);

app.factory('dressFactory', ['$resource', 'baseURL', function ($resource, baseURL) {
    return $resource(baseURL + "dress/:dressId", {
        dressId: "@dressId"
    }, {
        update: {
            method: "PUT"
        }
    });
}]);

app.factory('shirtsFactory', ['$resource', 'baseURL', function ($resource, baseURL) {
    return $resource(baseURL + "shirts/:shirtId", {
        shirtId: "@shirtId"
    }, {
        update: {
            method: "PUT"
        }
    });
}]);

app.factory('trousersFactory', ['$resource', 'baseURL', function ($resource, baseURL) {
    return $resource(baseURL + "trousers/:trouserId", {
        trouserId: "@trouserId"
    }, {
        update: {
            method: "PUT"
        }
    });
}]);

app.factory('userFactory', ['$resource', 'baseURL', function($resource,baseURL) {
    return $resource(baseURL + "users/login/:userId", {
        userId : "@userId"
    },{
        update : {
            method : "PUT"
        }
    });
}]);
